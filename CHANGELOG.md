
# Changelog for session checker (deprecated)

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.0-SNAPSHOT] - 2023-06-12

- ported to git

## [v1.0.0] - 2015-07-02

First release 
